(function($) {
  'use strict';

  //===== Prealoder

  $(window).on('load', function(event) {
    $('.preloader')
      .delay(500)
      .fadeOut(500);
  });

  //===== Mobile Menu

  $('.navbar-toggler').on('click', function() {
    $(this).toggleClass('active');
  });

  $('.navbar-nav a').on('click', function() {
    $('.navbar-toggler').removeClass('active');
  });

  //===== close navbar-collapse when a  clicked

  $('.navbar-nav a').on('click', function() {
    $('.navbar-collapse').removeClass('show');
  });

  //===== Sticky

  $(window).on('scroll', function(event) {
    var scroll = $(window).scrollTop();
    if (scroll < 10) {
      $('.navigation').removeClass('sticky');
    } else {
      $('.navigation').addClass('sticky');
    }
  });

  //===== Section Menu Active

  var scrollLink = $('.page-scroll');
  // Active link switching
  $(window).scroll(function() {
    var scrollbarLocation = $(this).scrollTop();

    scrollLink.each(function() {
      var sectionOffset = $(this.hash).offset().top - 73;

      if (sectionOffset <= scrollbarLocation) {
        $(this)
          .parent()
          .addClass('active');
        $(this)
          .parent()
          .siblings()
          .removeClass('active');
      }
    });
  });

  // Parallaxmouse js

  function parallaxMouse() {
    if ($('#parallax').length) {
      var scene = document.getElementById('parallax');
      var parallax = new Parallax(scene);
    }
  }
  parallaxMouse();

  //===== Progress Bar

  if ($('.progress-line').length) {
    $('.progress-line').appear(
      function() {
        var el = $(this);
        var percent = el.data('width');
        $(el).css('width', percent + '%');
      },
      { accY: 0 }
    );
  }

  //===== Counter Up

  $('.counter').counterUp({
    delay: 10,
    time: 1600,
  });

  //===== Magnific Popup

  $('.image-popup').magnificPopup({
    type: 'image',
    gallery: {
      enabled: true,
    },
  });

  //===== Back to top

  // Show or hide the sticky footer button
  $(window).on('scroll', function(event) {
    if ($(this).scrollTop() > 600) {
      $('.back-to-top').fadeIn(200);
    } else {
      $('.back-to-top').fadeOut(200);
    }
  });

  //Animate the scroll to yop
  $('.back-to-top').on('click', function(event) {
    event.preventDefault();

    $('html, body').animate(
      {
        scrollTop: 0,
      },
      1500
    );
  });

  //   SEND EMAIL ON BUTTON CLICK
  // =============================
  $('#send_btn').on('click', (e) => {
    e.preventDefault();
    submitToAPINew();
  });

  function submitToAPINew() {
    let name = document.querySelector('#name_input').value;
    let email = document.querySelector('#email_input').value;
    let enquiry = document.querySelector('#message_input').value;

    var Namere = /[A-Za-z]{1}[A-Za-z]/;
    if (!Namere.test(name)) {
      alert('Name can not less than 2 char');
      return;
    }
    var reeamil = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,6})?$/;
    if (!reeamil.test(email)) {
      alert('Please enter valid email address');
      return;
    }

    var payload = {
      name: name,
      email: email,
      enquiry: enquiry,
    };

    let xhr = new XMLHttpRequest();
    xhr.open(
      'POST',
      'https://q3oslt74q1.execute-api.ap-southeast-2.amazonaws.com/default/contactForm',
      true
    );
    xhr.send(JSON.stringify(payload));
    location.reload();
    alert('Message sent Successfully..!');
  }

  //=====
})(jQuery);
